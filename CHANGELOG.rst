Changelog
#########

relativetimebuilder 3.0.2-dev.0
===============================

*Release date: YYYY-MM-DD*

relativetimebuilder 3.0.1
=========================

*Release date: 2025-01-09*

Changed
-------
* Specify changelog location in :code:`setup.py`

Added
-----
* Development requirements handled by :code:`extras_require` (install with :code:`pip install -e .[dev]`)
* Pre-commit hooks, managed with `pre-commit <https://pre-commit.com/>`_ (install with :code:`pre-commit install`)
* Add :code:`readthedocs.yaml` to make configuration explicit

Changed
-------
* Code formatted with `Black <https://black.readthedocs.io/en/stable/index.html>`_
* Imports sorted with `isort <https://pycqa.github.io/isort/>`_
* Following `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_ for this and future CHANGELOG entries
* Heading level of top of CHANGELOG
* Increase supported aniso8601 version to <11.0.0

relativetimebuilder 3.0.0
=========================

*Release date: 2021-02-18*

Changes
-------
* Add support for concise interval format (e.g. "2007-12-14T13:30/15:30")
* Implement range checks supported by aniso8601 9.0.0
* Add version to :code:`version.py`
* Cleaner reading of `README.rst` into the :code:`long_description` field of `setup.py`
* Define :code:`long_description_content_type` as :code:`text/x-rst`
* Simplify Sphinx configuration
* Bump copyright date to 2021

Deprecation
-----------
* Deprecate running tests with :code:`python setup.py tests` as the test suite support in Setuptools is `deprecated <https://github.com/pypa/setuptools/issues/1684>`_

relativetimebuilder 2.0.1
=========================

*Release date: 2019-09-11*

Changes
-------
* Add support for `aniso8601 <https://bitbucket.org/nielsenb/aniso8601>`_ 8.0.0
* Fix semver usage for prelease version, as required by `clause 9 <https://semver.org/#spec-item-9>`_

relativetimebuilder 2.0.0
=========================

*Release date: 2019-06-11*

Changes
-------
* Support `aniso8601 <https://bitbucket.org/nielsenb/aniso8601>`_ 7.0.0
* Treat all fractional components as integer microseconds, see `aniso8601 issue #24<https://bitbucket.org/nielsenb/aniso8601/issues/24/float-induced-rounding-errors-when-parsing>`_ for details

relativetimebuilder 1.0.0
=========================

*Release date: 2019-03-08*

Changes
-------
* Update classifier to "Production/Stable"
* Support `aniso8601 <https://bitbucket.org/nielsenb/aniso8601>`_ 6.0.0
* Explicitly require `python-dateutil <https://pypi.python.org/pypi/python-dateutil>`_

relativetimebuilder 0.2.0
=========================

*Release date: 2019-03-01*

Changes
-------
* Support `aniso8601 <https://bitbucket.org/nielsenb/aniso8601>`_ 5.0.0
* Fractional arguments are now handled with greater precision (`discussion <https://bitbucket.org/nielsenb/aniso8601/issues/21/sub-microsecond-precision-is-lost-when>`_)

relativetimebuilder 0.1.0
=========================

*Release date: 2019-01-09*

Changes
-------
* Make tests importable
* Remove support for distutils
* Run tests with setuptools
* Initial Read the Docs support

relativetimebuilder 0.0.1
=========================

*Release date: 2018-10-25*

Changes
-------
* Initial release
